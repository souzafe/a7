<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Assignment 7 - Customer Details</title>
        <link rel="stylesheet" href="css/css.css" />
<!-- 
File name: A7.php
Author: Fernando Rafael Souza
Version: 1.0
Description: Project for Assignment 7
-->
    </head>
    <body>
        <?php
        
        if(isset($_POST['submit'])){            
            $fname = $_POST['fname'];
            $lname = $_POST['lname'];
            $email = $_POST['email'];
            $date = $_POST['date'];
            $runner = $_POST['runner'];
            $terrain = $_POST['terrain'];
            $rate = $_POST['rate'];
        }
        
        ?>

        <div>
            
            <h1>Customer Details</h1>
            
            <form action="content/address.php" method="post">
                First name:
                <input title="fname" type="text" name="fname" value="<?php echo $fname?>" maxlength="50" required><br /><br />
                
                Last name:
                <input title="lname" type="text" name="lname" value="<?php echo $lname?>" maxlength="50" required><br /><br />
                
                Email:
                <input title="email" type="email" name="email" value="<?php echo $email?>" required><br /><br />
                
                Date:
                <input title="date" type="date" name="date" required><br /><br />
                
                Runner:<br />                 
                <input type="radio" name="runner[]" id="runnery" value="yes"> 
                <label for="runnery">Yes</label><br />
                <input type="radio" name="runner[]" id="runnern" value="no"> 
                <label for="runnern">No</label><br /><br />
                
                Your Favorite terrain: <br />
                <input type="radio" name="terrain" value="road" id="road" <?php if($terrain == "road"){echo "checked";} ?>>
                <label for="road">Road</label><br />
                <input type="radio" name="terrain" value="track" id="track" <?php if($terrain == "track"){echo "checked";} ?>>
                <label for="track">Track</label><br />
                <input type="radio" name="terrain" value="trail" id="trail" <?php if($terrain == "trail"){echo "checked";} ?>>
                <label for="trail">Trail</label><br /><br />
                
                Rank your current shoes from 1 to 10: <span class="error"><br />
                <input title="rate" type="number" name="rate" min="1" max="10" value="0"><br /><br />
                
                <input type="submit" value="Submit" name=submit>
            </form>                       
        </div>
    </body>
</html>
