<?php        
        session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Assignment 7 - Completed</title>
        <link rel="stylesheet" href="../css/css.css" />
    </head>
    <body>
        <div>
        
        <h1>Order Completed</h1><br />
        <p>Order Successfully placed.</p>
        
        <?php

        // destroy the session
        session_destroy();
        ?>

        
        <p>Session data <strong>destroyed</strong>!</p><br />
        
        <a href="../index.php">Home Page</a>
        </div>
        

    </body>
</html>
