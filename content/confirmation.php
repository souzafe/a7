<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Assignment 7 - Confirmation Page</title>
        <link rel="stylesheet" href="../css/css.css" />
    </head>
    <body>
        <h1>confirmation Page</h1>
        <?php
        #Initialize Variables and Session Variables
        $rate = array();
        
            $fname = $_SESSION['fname'];
            $lname = $_SESSION['lname'];
            $email = $_SESSION['email'];
            $date = $_SESSION['date'];
            $runner = $_SESSION['runner'];
            $terrain = $_SESSION['terrain'];
            $rate = $SESSION["rate"];
            
            $add = $_SESSION['add'];
            $city = $_SESSION['city'];
            $prov = $_SESSION['prov'];
            $zip = $_SESSION['zip'];
            
            $ship = $_SESSION['shipping'] = $_post['shipping'];        
        ?>
        
        <div id="conf">
            <h1>Confirmation Page</h1><br />
            <h2>Customer Details</h2>
            <p> First Name: <strong><?php echo $fname ?></strong></p>
            <p> Last Name: <strong><?php echo $lname ?></strong></p>
            <p> Email: <strong><?php echo $email ?></strong></p>
            <p> Date: <strong><?php echo $date ?></strong></p>
            <p> Runner: <strong><?php echo $runner[0] ?></strong></p>
            <p> Favorite Terrain: <strong><?php echo $terrain ?></strong></p>
            <p> Shoes Rating: <strong><?php echo $rate ?></strong></p><br />
            
            <h2>Address Details</h2>
            <p> Address: <strong><?php echo $add ?></strong></p>
            <p> City: <strong><?php echo $city ?></strong></p>
            <p> Province: <strong><?php echo $prov ?></strong></p>
            <p> Zip Code: <strong><?php echo $zip ?></strong></p><br />

            <h2>Shipping Details</h2>
            <p> Shipping Method: <strong><?php echo $ship; ?></strong></p>


            <form action="completed.php" method="post">
                <input type="submit" value="Submit" name=submitconf>
            </form>
        </div>
    </body>